(function() {	
	'use strict';

  var sqrt  = Math.sqrt
    , cos   = Math.cos
    , sin   = Math.sin
    , atan2 = Math.atan2
    , random = Math.random;

  var Vector = function(options) {
    this.x  = options.x;
    this.y  = options.y;

    this.origin = {x: this.x, y: this.y};
    
    this.bounciness = options.bounciness || -1;
    this.friction   = options.friction || 1;
    this.mass       = options.mass || 1;
    this.radius     = options.radius || 0;

    var direction   = options.direction || 0;
    var speed       = options.speed || 0;

    this.vx = cos(direction) * speed;
    this.vy = sin(direction) * speed;

    this.springs      = [];
  };

  Vector.prototype.updatePhysics = function() {
    this.handleSprings();
    this.vx *= this.friction;
    this.vy *= this.friction;
    this.x += this.vx;
    this.y += this.vy;
    // this.x >>= 0;
    // this.y >>= 0;
  };

  Vector.prototype.addSpring = function(point, k, length) {
    this.removeSpring(point);
    this.springs.push({
      point: point,
      k: k,
      length: length || 0
    });
  };

  Vector.prototype.removeSpring = function(point) {
    for (var i = 0; i < this.springs.length; i += 1) {
      if (point === this.springs[i].point) {
        this.springs.splice(i, 1);
        return;
      }
    }
  };

  Vector.prototype.getSpeed = function() {
    return sqrt(this.vx * this.vx + this.vy * this.vy);
  };

  Vector.prototype.setSpeed = function(speed) {
    var heading = this.getHeading();
    this.vx     = cos(heading) * speed;
    this.vy     = sin(heading) * speed;
  };

  Vector.prototype.getHeading = function() {
    return atan2(this.vy, this.vx);
  };

  Vector.prototype.setHeading = function(heading) {
    var speed = this.getSpeed();
    this.vx   = cos(heading) * speed;
    this.vy   = sin(heading) * speed;
  };

  Vector.prototype.angleTo = function(p2) {
    return atan2(p2.y - this.y, p2.x - this.x);
  };

  Vector.prototype.distanceTo = function(p2) {
    var dx = p2.x - this.x
      , dy = p2.y - this.y;

    return sqrt((dx * dx) + (dy * dy));
  };

  Vector.prototype.handleSprings = function() {
    for (var i = 0; i < this.springs.length; i += 1) {
      var spring = this.springs[i];
      this.springTo(spring.point, spring.k, spring.length);
    }
  };

  Vector.prototype.springTo = function(point, k, length) {
    var dx = point.x - this.x
      , dy = point.y - this.y
      , distance = sqrt(dx * dx + dy * dy)
      , springForce = (distance - length || 0) * k

      , vx = this.vx + dx / distance * springForce
      , vy = this.vy + dy / distance * springForce;

    this.vx = isNaN(vx) ? 0 : vx;
    this.vy = isNaN(vy) ? 0 : vy;
  };

	module.exports = Vector;
	
})(this);
