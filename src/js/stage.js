(function() {
  'use strict';

  var config = require('../../shared/config.js')
    , utils = require('../../shared/utils.js')
    , Renderable = require('./shapes/renderable.js')
    , starField = require('./shapes/starfield.js')
    , blobGroup = require('./shapes/blobgroup.js')
    , getColorValues = utils.color.getValues;

  var Stage = function() {
    var area = config.area;
    this.canvas = document.createElement('canvas');
    this.context = this.canvas.getContext('2d');

    this.width  = this.canvas.width  = area[0];
    this.height = this.canvas.height = area[1];

    this.starField = starField({
      numStars: 200,
      area: area
    });

    this.blobGroup = blobGroup({
      numBlobs: 4,
      area: area
    });
    
    Renderable.context = this.context;
    this.setSize();
    window.addEventListener('resize', this.setSize.bind(this));
    document.body.appendChild(this.canvas);
  };

  Stage.prototype.render = function() {
    this.context.clearRect(0, 0, this.width, this.height);
    this.starField.draw(this.context);
    this.blobGroup.draw();
  };

  Stage.prototype.setCollision = function(color) {
    this.blobGroup.startColorCycleTime = Date.now();
    this.blobGroup.currentHue = getColorValues(this.blobGroup.color)[0];
    this.blobGroup.collisionHue = getColorValues(color)[0];
  };

  Stage.prototype.setSize = function() {
    var ww = window.innerWidth;
    Renderable.context.zoom = this.canvas.zoom = config.area[0] / ww;
  };

  module.exports = Stage;

})(this);