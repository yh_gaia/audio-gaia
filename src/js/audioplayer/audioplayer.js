(function() {
  'use strict';

  var Synth = require('./synth.js')
  ,   utils = require('../../../shared/utils.js');

  module.exports = function(audiolet) {
    var frequencies = [
      261.626, 
      293.665, 
      329.628, 
      391.995, 
      440, 
      523.251, 
      587.330, 
      659.255,
      783.991, 
      880.000
    ];

    var getRandomInt = utils.random.getInt;

    return {
      play: function(sound) {
        var synth = new Synth(audiolet);
        var wf = sound.waveform;
        var decay = sound.decay;
        var id = sound.id;

        if (wf === 'bass') {
          synth.play(wf, decay, frequencies[id], 200, 1000);
        } else {
          synth.play(wf, decay, frequencies[id], 400, 1000);
        }
      },

      sequence: function() {
        audiolet.scheduler.play(1, 3, function() {
          var frequency = frequencies[getRandomInt(0, 9)];
          var synth = new Synth(audiolet);
          synth.play('triangle', 2.5, frequency, 2000, 2500);
        });
      }
    };
  };

})(this);
