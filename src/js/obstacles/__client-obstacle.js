(function() {
  'use strict';

  var ObstacleBase = require('./client-obstacle-base-model.js')
    , Shape    = require('../shapes/__shape.js')
    , inherits    = require('../../../shared/utils.js').inherits;

  var SharpForm = function(data) {
    ObstacleBase.call(this, data);
    this.shape = new Shape.Polygon(data);
    this.shape.setUpFade();
  };

  inherits(SharpForm, ObstacleBase);

  var Bassform = function(data) {
    ObstacleBase.call(this, data);
    this.shape = new Shape.Spiky(data);
    this.shape.setUpFade();
    this.angle = 0;
  };

  inherits(Bassform, ObstacleBase);

  var Noiseform = function(data) {
    ObstacleBase.call(this, data);
    this.shape = new Shape.Star(data);
    this.shape.setUpFade();
  };

  inherits(Noiseform, ObstacleBase);

  var RoundForm = function(data) {
    ObstacleBase.call(this, data);
    this.shape = new Shape.Circle(data);
    this.shape.setUpFade();
  };

  inherits(RoundForm, ObstacleBase);

  module.exports = {
    Collection : require('./client-obstacle-collection.js'),
    BassForm   : Bassform,
    SharpForm  : SharpForm,
    RoundForm  : RoundForm,
    NoiseForm  : Noiseform
  };

})(this); 