(function() {
  'use strict';

  var Vector = require('../vector.js')
    , utils = require('../../../shared/utils.js');

  var ClientObstacleBaseModel = function(data) {
    this.type       = data.type;
    this.id         = data.id;
    this.didCollide = false;
    this.willFadeIn = true;
    
    this.fillTimer   = 1;
    this.updateHz    = 0.02;

    this.position = new Vector({
      x         : data.x,
      y         : data.y,
      direction : data.direction,
      speed     : 1,
      friction  : 0
    });

    this.boundary = {
      top    : data.y - data.radius * 0.5,
      left   : data.x - data.radius * 0.5,
      bottom : data.y + data.radius * 0.5,
      right  : data.x + data.radius * 0.5
    };

    this.sendDeadEvent = new CustomEvent('dead', {
      'detail': {id: null, type: null}
    });
  };

  ClientObstacleBaseModel.prototype.onCollision = function() {
    this.shape.willFadeOut = this.didCollide;

    if (this.didCollide) {
      this.shape.isFilled = true;

      this.fillTimer -= 0.05;
      if (this.fillTimer < 0) {this.shape.isFilled = false;}
      if (this.shape.alpha < 0.05) {        
        this.sendDeadEvent.detail.id   = this.id;
        this.sendDeadEvent.detail.type = this.type;
        document.dispatchEvent(this.sendDeadEvent);

        this.shape.alpha = 0;
        this.didCollide  = false;
      }
    }
  };

  ClientObstacleBaseModel.prototype.update = function() {
    this.rotate();
    this.wiggle();
    this.onCollision();
    this.position.updatePhysics();

    this.shape.x = this.position.x;
    this.shape.y = this.position.y;
  };

  ClientObstacleBaseModel.prototype.rotate = function() {
    this.shape.rotation += this.updateHz;
    if (this.shape.rotation > 360) {this.shape.rotation = 0;}
  };

  ClientObstacleBaseModel.prototype.wiggle = function() {
    var p = this.position;
    var x = p.x;
    var y = p.y;
    var r = p.radius;
    var b = -1;
    var right  = this.boundary.right;
    var left   = this.boundary.left;
    var top    = this.boundary.top;
    var bottom = this.boundary.bottom;

    if (x + r > right) {
      p.x = right - r;
      p.vx *= b;
    }
    if (x - r < left) {
      p.x = r + left;
      p.vx *= p.bounciness;
    }
    if (y + r > bottom) {
      p.y = bottom - r;
      p.vy *= b;
    }
    if (y - r < top) {
      p.y = r + top;
      p.vy *= b;
    }
  };

  module.exports = ClientObstacleBaseModel;

})(this);