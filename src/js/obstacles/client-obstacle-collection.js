(function() {
  'use strict';

  module.exports = function(obstacles) {
    var oc = obstacles;

    return {
      spawn: function(obstacles) {
        for (var type in obstacles) {
          for (var id in obstacles[type]) {
            var ot = oc[type];
            ot[id] = new ot.model(obstacles[type][id]);
          }
        }
      },

      resurrect: function(data) {
        var ot = oc[data.type + 's'];
        ot[data.id] = new ot.model(data);
        return ot[data.id];
      },

      setCollision: function(data) {
        oc[data.type + 's'][data.id].didCollide = true;
        oc[data.type + 's'][data.id].shape.startFadeOutTime = new Date();
      },

      update: function() {
        for (var type in oc) {
          for (var id in oc[type]) {
            if (id !== 'model') { oc[type][id].update(); }
          }
        }
      },

      draw: function() {
        for (var type in oc) {
          for (var id in oc[type]) {
            if (id !== 'model') { oc[type][id].shape.draw(); }
          }
        }
      }

    };
  };

})(this);