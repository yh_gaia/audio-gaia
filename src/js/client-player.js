(function() {
  'use strict';

  var shape          = require('./shapes/__shape.js')
    , Vector         = require('./vector.js')
    , config         = require('../../shared/config.js')
    , utils          = require('../../shared/utils.js')
    , inherits       = utils.inherits
    , getColorValues = utils.color.getValues
    , getColor       = utils.color.get
    , easeOut        = require('./easing.js').easeOut

    , sin = Math.sin;

  // =============================================================
  // CLIENT PLAYER MODEL
  // =============================================================
  var ClientPlayer = function(data) {
    this.id = data.id;

    this.position = new Vector({
      x         : data.x,
      y         : data.y,
      direction : 45,
      friction  : 0.4
    });

    this.head = new shape.Circle({
      x        : data.x,
      y        : data.y,
      color    : data.color,
      radius   : data.radius,
      isFilled : true
    });

    this.tail = shape.Tail({
      points    : 8,
      origin    : [this.position.x, this.position.y],
      direction : this.position.direction,
      friction  : this.position.friction,
      stiffness : 0.6
    });

    this.angle = 0;
    this.updateHz = 0.05;

    this.head.setUpFade();

    this.springPoint = {x: data.x, y: data.y};
    this.position.addSpring(this.springPoint, 0.1);
    this.tail.addSpringsTo(this.position);

    this.willBeRemoved = false;

    this.sendRemoveEvent = new CustomEvent('removePlayer', {
      'detail': {id: this.id}
    });
  };

  ClientPlayer.prototype.send = function() {
    return {
      id          : this.id,
      radius      : this.head.radius,
      x           : this.position.x, 
      y           : this.position.y,
      springPoint : this.springPoint
    };
  };

  ClientPlayer.prototype.update = function() {
    this.pulse();
    this.position.updatePhysics();
    this.tail.update();

    this.head.x = this.position.x;
    this.head.y = this.position.y;
  };

  ClientPlayer.prototype.draw = function() {
    if (this.newHue) { this.cycleColor(); }
    this.head.draw();
    this.tail.lineWidth = this.head.radius * 0.8 + sin(this.angle) * 2;
    this.tail.draw(this.head.color);
    this.tail.alpha = this.head.alpha;

    if (this.willBeRemoved && this.head.alpha < 0.1) {
      this.head.alpha = 0;
      document.dispatchEvent(this.sendRemoveEvent);
    }
  };

  ClientPlayer.prototype.cycleColor = function() {
    var t = Date.now() - this.startColorCycleTime;
    var d = 500;
    var currHue = this.currHue;
    var hueChange = currHue - this.newHue;

    if (t < d) {
      var newHue = easeOut(t, currHue, hueChange, d);
      this.head.color = getColor(newHue >> 0, 60, 60);
    }
  };

  ClientPlayer.prototype.setFadeOut = function() {
    this.tail.alpha = 0;
    this.head.willFadeOut = true;
    this.head.startFadeOutTime = Date.now();
  };

  ClientPlayer.prototype.pulse = function() {
    this.head.scale = 2 + sin(this.angle) * 0.5;
    this.angle += this.updateHz;
  };

  ClientPlayer.prototype.move = function(position) {
    this.springPoint.x = position.x;
    this.springPoint.y = position.y;
  };

  // =============================================================
  // CLIENT PLAYER COLLECTION :: extends BASECOLLECTION
  // =============================================================

  var ClientPlayerCollection = function(options) {
    var Model = options.model;
    var collection = {};

    return {
      fadeOutPlayer: function(id) {
        collection[id].willBeRemoved = true;
        collection[id].setFadeOut();
      },

      remove: function(id) {
        collection[id] = null;
        delete collection[id];
      },

      draw: function() {
        for (var id in collection) {
          collection[id].draw();
        }
      },

      update: function() {
        for (var id in collection) {
          collection[id].update();
        }
      },

      setCollision: function(id, color) {
        var player = collection[id];
        player.didCollide = true;
        player.currHue = getColorValues(player.head.color)[0];
        player.newHue = getColorValues(color)[0];
        player.startColorCycleTime = Date.now();
      },

      add: function(data) {
        collection[data.id] = new Model(data);
      },

      updatePlayer: function(data) {
        collection[data.id].move(data.springPoint);
      },

      set: function(players) {
        for (var id in players) {
          collection[id] = new Model(players[id]);
        }
      },

      get: function(id) {
        if (typeof id === 'undefined') {
          return collection;
        }
        return collection[id];
      }
    };
  };

  module.exports = {
    Model: ClientPlayer,
    Collection: ClientPlayerCollection
  };

})(this);