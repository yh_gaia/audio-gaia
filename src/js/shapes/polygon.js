(function() {
  'use strict';

  var BaseShape = require('./_base-shape.js')
    , utils    = require('../../../shared/utils.js')
    , inherits = utils.inherits;

  var Polygon = function(options) {
    BaseShape.call(this, options);
  };
  inherits(Polygon, BaseShape);

  Polygon.prototype.draw = function() {
    var context = this.context;
    this.beginDraw();

    for (var i = 0; i < this.vertices.length; i++) {
      var x = this.vertices[i].x;
      var y = this.vertices[i].y;
      if (i === 0) {
        context.moveTo(x, y);
      } else {
        context.lineTo(x, y);
      }
    }
    context.closePath();
    this.endDraw();
  };

  module.exports = Polygon;

})(this);