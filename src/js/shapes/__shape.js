(function() {
  'use strict';

  module.exports = {
    Polygon   : require('./polygon.js'),
    Circle    : require('./circle.js'),
    Star      : require('./star.js'),
    Spiky      : require('./spiky.js'),
    Tail      : require('./tail.js'),
    StarField : require('./starfield.js')
  };

})(this);