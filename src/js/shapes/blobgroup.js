(function() {
  'use strict';

  var utils = require('../../../shared/utils.js')
    , easing = require('../easing.js')
    , Vector = require('../vector.js')
    , Polygon = require('./polygon.js');

  module.exports = function(options) {
    var floor = Math.floor;
    var random = Math.random;
    var easeOut = easing.easeOut;
    var getColor = utils.color.get;
    var getRandomInt = utils.random.getInt;
    var getIrregularPolygon = utils.vertices.getIrregularPolygon;

    var w = options.area[0];
    var h = options.area[1];
    var numBlobs = options.numBlobs;
    var blobs = [];

    return {
      color: 'hsl(50,0%,20%)',
      collisionHue: null,
      currentHue: null,

      draw: function() {
        if (blobs.length < numBlobs) {
          var r = getRandomInt(h * 0.2, h * 0.25);
          blobs.push(new Polygon({
            x        : w * 0.5,
            y        : h * 0.5,
            alpha    : 0.1,
            radius   : r,
            color    : this.color,
            isFilled : true,
            updateHz : random() * (h * 0.00001),
            vertices : getIrregularPolygon(getRandomInt(4, 8), r),
            shadow   : false
          }));
        }

        if (this.collisionHue) {this.cycleColor();}

        for (var i = 0; i < blobs.length; i++) {
          var b = blobs[i];
          b.color = this.color;
          b.rotation += b.updateHz;
          b.draw();
        }
      },

      cycleColor: function() {
        var t = Date.now() - this.startColorCycleTime;
        var d = 1000;
        var currHue = this.currentHue;
        var hueChange = currHue - this.collisionHue;

        if (t < d) {
          var newHue = easeOut(t, currHue, hueChange, d);
          this.color = getColor(newHue >> 0, 60, 20);
        }
      }
    };
  };

})(this);