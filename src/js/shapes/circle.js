(function() {
  'use strict';

  var BaseShape = require('./_base-shape.js')
    , inherits  = require('../../../shared/utils.js').inherits;

  var PI2 = Math.PI * 2;

  var Circle = function(options) {
    BaseShape.call(this, options);
  };
  inherits(Circle, BaseShape);

  Circle.prototype.draw = function() {
    this.beginDraw();
    this.context.arc(0, 0, this.radius, 0, PI2, false);
    this.endDraw();
  };

  module.exports = Circle;

})(this);