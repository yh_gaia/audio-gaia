(function() {
  'use strict';

  var BaseShape = require('./_base-shape.js')
    , utils    = require('../../../shared/utils.js')
    , inherits = utils.inherits
    , PI = Math.PI
    , PI2 = PI * 2
    , cos = Math.cos
    , sin = Math.sin;

  var Spiky = function(options) {
    BaseShape.call(this, options);
    this.angle = 0;
    this.updateHz = 0.05;
  };
  inherits(Spiky, BaseShape);

  Spiky.prototype.draw = function() {
    var sides = this.points
      , a = PI2 / sides
      , context = this.context
      , r = this.radius;
    
    this.beginDraw();

    context.moveTo(r, 0);
    for (var i = 1; i < sides; i++) {
      context.lineTo(r * cos(a * i), r * sin(a * i));
    }
    context.closePath();

    var angle = 0, radians, x, y;
    for (var j = 0; j < sides; j++) {
      radians = angle / 180 * PI;
      x = r * cos(radians);
      y = r * sin(radians);

      context.moveTo(x, y);
      context.lineTo(x * 1.25, y * 1.25);

      angle += 360/sides;
    }
    this.endDraw();
  };

  module.exports = Spiky;

})(this);