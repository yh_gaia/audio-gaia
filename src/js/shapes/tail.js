(function() {
  'use strict';

  var Vector = require('../vector.js')
    , Renderable = require('./renderable.js');

  module.exports = function(options) {
    var numPoints = options.points;
    var direction = options.direction;
    var friction  = options.friction;
    var k         = options.stiffness;
    var points    = [];

    var ox = options.origin[0];
    var oy = options.origin[1];

    var context = Renderable.context;

    for (var i = 0; i < numPoints; i++) {
      points.push(new Vector({
        x         : ox,
        y         : oy,
        direction : direction,
        friction  : friction
      }));
    }

    return {
      lineWidth: 1,
      alpha: 1,

      addSpringsTo: function(object) {
        for (var i = 0; i < points.length; i++) {
          if (i === 0) {
            points[i].addSpring(object, k);
          } else {
            points[i].addSpring(points[i - 1], k);
          }
        }
      },

      draw: function(color) {
        var p;

        context.save();
        context.globalAlpha = this.alpha;
        context.beginPath();

        for (var i = 0; i < points.length; i++) {
          p = points[i];
          context.lineTo(p.x, p.y);
        }

        context.lineWidth   = this.lineWidth;
        context.lineCap     = 'round';
        context.strokeStyle = color;
        context.shadowColor = color;
        context.shadowBlur  = 20;
        context.stroke();
        context.restore();
      },

      update: function() {
        var p;
        for (var i = 0; i < points.length; i++) {
          p = points[i];
          p.updatePhysics();
        }
      }
    };
  };
  
})(this);