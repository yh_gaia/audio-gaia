(function() {
  'use strict';

  var utils = require('../../../shared/utils.js')
    , Vector = require('../vector.js');

  module.exports = function(options) {
    var rand         = utils.random.get;
    var getRandomInt = utils.random.getInt;
    var PI2          = Math.PI * 2;
    var random       = Math.random;

    var numStars = options.numStars;
    var area     = options.area;
    var stars    = [];

    var updateHeading = function(heading) {
      this.heading = heading;
    };

    var draw = function(context) {
      var w = area[0]
        , h = area[1]
        , heading = this.heading
        , star, x, y, r, a;

      if (stars.length < numStars) {
        var s = new Vector({
          x         : getRandomInt(0, w),
          y         : getRandomInt(0, h),
          radius    : random() * (h * 0.01),
          speed     : random() * (h * 0.003),
          direction : heading
        });
        s.alpha = rand() * 0.2;
        stars.push(s);
      }

      for (var i = 0; i < stars.length; i++) {
        star = stars[i];
        r = star.radius;
        x = star.x;
        y = star.y;
        a = star.alpha;

        star.setHeading(this.heading);
        star.updatePhysics();
        context.save();
        context.beginPath();

        context.arc(x, y, r, 0, PI2, false);
        context.fillStyle = 'rgba(210,210,210,'+ a +')';
        context.fill();
        context.restore();

        if (y - (r * 2) > h) { star.y = -(r * 2); }
        if (x - (r * 2) > w) { star.x = -(r * 2); }
        if (y + (r * 2) < 0) { star.y = h + (r * 2); }
        if (x + (r * 2) < 0) { star.x = w + (r * 2); }
      }
    };

    return {
      updateHeading: updateHeading,
      draw: draw,
      heading: 45
    };
  };

})(this);