(function() {
  'use strict';

  module.exports = function(options) {
    var element = options.element;
    var loader = options.loader;

    return {
      playBtn: options.playBtn,
      
      onLoad: function() {
        loader.className = 'off';
        this.playBtn.className = 'on';
      },

      fade: function(delay) {
        element.className = 'off';
        setTimeout(function() {
          element.style.display = 'none';
          element.innerHTML = '';
        }, delay);
      }
    };
  };

})(this);