(function() {
  'use strict';

  var io = require('socket.io-client')
    , Stage = require('./stage.js')
    , player = require('./client-player.js')
    , obstacle = require('./obstacles/__client-obstacle.js')
    , Game = require('./client-game.js')
    , audioPlayer = require('./audioplayer/audioplayer.js')
    , splash = require('./splash.js');

  function isCanvasSupported(){
    var elem = document.createElement('canvas');
    return !!(elem.getContext && elem.getContext('2d'));
  }

  if (isCanvasSupported) {
    var game = new Game({
      stage: new Stage(),
      players: player.Collection({model: player.Model}),
      audioplayer: audioPlayer(new Audiolet()),
      obstacles: obstacle.Collection({
        bassForms  : {model: obstacle.BassForm},
        noiseForms : {model: obstacle.NoiseForm},
        sharpForms : {model: obstacle.SharpForm},
        roundForms : {model: obstacle.RoundForm}
      })
    });

    var mySplash = splash({
      element: document.getElementById('splash'),
      loader: document.getElementById('loader'),
      playBtn: document.getElementById('play')
    });
    
    window.addEventListener('load', function(event) {
      game.start(io());
      // mySplash.onLoad();
      // mySplash.playBtn.addEventListener('click', function(event) {
      //   event.preventDefault();
      //   mySplash.fade(3000);
      //   mySplash = null;
      //   game.start(io());
      // });
    });

  } else {
    console.error('Sorry, but your browser doesn\'t seem to support the HTML5 Canvas element :(');
  }
  
})(this);
