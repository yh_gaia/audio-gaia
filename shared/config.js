(function() {
  'use strict';

  var ratioFactor = [0.625, 1.6];
  var width = 1800;
  var height = width * ratioFactor[0];
  var sizeFactor = width / 800;

  module.exports = {
    area: [width, height],

    sizeFactor: sizeFactor,
    
    angleArray: [-125, -45, 45, 125],

    color: {
      range: [0, 360],
      saturation: 60,
      luma: 60
    },

    sound: {
      decayRange: [
        7 * sizeFactor, 
        8 * sizeFactor, 
        9 * sizeFactor, 
        10 * sizeFactor, 
        11 * sizeFactor, 
        12 * sizeFactor, 
        13 * sizeFactor, 
        14 * sizeFactor, 
        15 * sizeFactor, 
        16 * sizeFactor
      ]
    },

    player: {
      radius: 5,
      color: 'hsl(180,0%,100%)'
    },

    forms: {
      radiusRange: [7, 16],
      sharp: {
        amount: 10,
        waveform: 'saw'
      },
      round: {
        amount: 10,
        waveform: 'sine'
      },
      noise: {
        amount: 10,
        waveform: 'noise'
      },
      bass: {
        amount: 10,
        waveform: 'bass'
      } 
    }
  };

})(this);